import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

import { Supplier } from '../../models/supplier';
import { SupplierService } from '../../services/supplier.service';

@Component({
  selector: 'app-suppliers',
  templateUrl: './suppliers.component.html',
  styleUrls: ['./suppliers.component.css']
})
export class SuppliersComponent implements OnInit {

  suppliers: Supplier[];
  isLoggedIn: boolean;
  isAdmin: boolean;

  constructor(
    private supplierService: SupplierService,
    private cookieService: CookieService
    ) { }

  ngOnInit(): void {
    this.isLoggedIn = this.cookieService.get('user') !== '';
    this.isAdmin = this.cookieService.get('role') === '1';
    if (!this.isLoggedIn) {
        window.location.replace('/login');
    }
    else {
      this.getSuppliers();
    }
  }

  getSuppliers(): void {
    this.supplierService.getSuppliers()
      .subscribe(suppliers => {
        if (this.isAdmin) {
          this.suppliers = suppliers;
        } else {
          this.suppliers = suppliers.filter(s => s.active);
        }
      });
  }

  enable(supplier: Supplier) {
    supplier.active = true;
    this.supplierService.updateSupplier(supplier).subscribe();
  }

  disable(supplier: Supplier){
    if (!this.isAdmin) {
      this.suppliers = this.suppliers.filter(s => s !== supplier);
    }
    supplier.active = false;
    this.supplierService.updateSupplier(supplier).subscribe();
  }
}
