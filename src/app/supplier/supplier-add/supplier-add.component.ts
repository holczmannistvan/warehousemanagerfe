import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { Supplier } from '../../models/supplier';
import { SupplierService } from '../../services/supplier.service';

@Component({
  selector: 'app-supplier-add',
  templateUrl: './supplier-add.component.html',
  styleUrls: ['./supplier-add.component.css']
})
export class SupplierAddComponent implements OnInit {

  private suppliers: Supplier[];
  supplierForm = new FormGroup({
    name: new FormControl('', Validators.required),
    accountNumber: new FormControl(),
    billingAddress: new FormControl()
  });

  constructor(private supplierService: SupplierService) { }

  ngOnInit(): void {
    this.getSuppliers();
  }

  getSuppliers(): void {
    this.supplierService.getSuppliers()
      .subscribe(suppliers => this.suppliers = suppliers);
  }

  findSupplierByName(name: string): Supplier{
    return this.suppliers.find(s => s.name === name);
  }

  add(): void {
    const name = this.supplierForm.get('name').value;
    const accountNumber = this.supplierForm.get('accountNumber').value;
    const billingAddress = this.supplierForm.get('billingAddress').value;
    const active = true;

    const availSupplier = this.findSupplierByName(name);

    if (availSupplier !== undefined){
      window.location.replace('/suppliers/detail/' + availSupplier.id);
    } else {
      this.supplierService.addSupplier({ active, name, accountNumber, billingAddress } as unknown as Supplier)
        .subscribe();

      window.location.replace('/suppliers');
    }
  }
}
