import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { CookieService } from 'ngx-cookie-service';

import { Supplier } from '../../models/supplier';
import { SupplierService } from '../../services/supplier.service';

@Component({
  selector: 'app-supplier-detail',
  templateUrl: './supplier-detail.component.html',
  styleUrls: ['./supplier-detail.component.css']
})
export class SupplierDetailComponent implements OnInit {

  @Input() supplier: Supplier;
  isLoggedIn: boolean;

  constructor(
    private route: ActivatedRoute,
    private supplierService: SupplierService,
    private location: Location,
    private cookieService: CookieService
  ) { }

  ngOnInit(): void {
    this.isLoggedIn = this.cookieService.get('user') !== '';
    if (!this.isLoggedIn) {
        window.location.replace('/login');
    }
    else {
      this.getSupplier();
    }
  }

  getSupplier(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.supplierService.getSupplier(id)
      .subscribe(supplier => this.supplier = supplier);
  }

  goBack(): void {
    this.location.back();
  }

  save(): void {
    this.supplierService.updateSupplier(this.supplier)
      .subscribe(() => this.goBack());
  }
}
