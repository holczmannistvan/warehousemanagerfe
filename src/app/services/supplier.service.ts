import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Supplier } from '../models/supplier';
import { MessageService } from './message.service';

@Injectable({
  providedIn: 'root'
})
export class SupplierService {

  private suppliersUrl = 'http://localhost:8080/suppliers';

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json'})
  };

  constructor(
    private http: HttpClient,
    private messageService: MessageService) { }

  getSuppliers(): Observable<Supplier[]> {
    return this.http.get<Supplier[]>(this.suppliersUrl)
      .pipe(
        tap(_ => this.log('fetchedSuppliers')),
        catchError(this.handleError<Supplier[]>('getSupppliers', []))
      );
  }

  getSupplier(id: number): Observable<Supplier> {
    const url = `${this.suppliersUrl}/${id}`;
    return this.http.get<Supplier>(url)
    .pipe(
      tap(_ => this.log(`fetched supplier id=${id}`)),
      catchError(this.handleError<Supplier>(`getSupplier id=${id}`))
    );
  }

  updateSupplier(supplier: Supplier): Observable<any> {
    const url = `${this.suppliersUrl}/${supplier.id}`;
    return this.http.put(url, supplier, this.httpOptions)
    .pipe(
      tap(_ => this.log(`updated supplier id=${supplier.id}`)),
      catchError(this.handleError<any>('updateSupplier'))
    );
  }

  addSupplier(supplier: Supplier): Observable<Supplier>{
    return this.http.post<Supplier>(this.suppliersUrl, supplier, this.httpOptions).pipe(
      tap((newSupplier: Supplier) => this.log(`added supplier w/ id=${newSupplier.id}`)),
      catchError(this.handleError<Supplier>('addSupplier'))
    );
  }

  private log(message: string) {
    this.messageService.add(`SupplierService: ${message}`);
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      this.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }
}
