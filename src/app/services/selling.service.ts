import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Selling } from '../models/selling';
import { MessageService } from './message.service';

@Injectable({
  providedIn: 'root'
})
export class SellingService {

  // TODO pageble hasznalatkor kihamozni az selling[]-t
  private sellingsUrl = 'http://localhost:8080/sellings';

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json'})
  };

  constructor(
    private http: HttpClient,
    private messageService: MessageService) { }

  getSellings(): Observable<Selling[]> {
    return this.http.get<Selling[]>(this.sellingsUrl)
      .pipe(
        tap(_ => this.log('fetchedsellings')),
        catchError(this.handleError<Selling[]>('getSellings', []))
      );
  }

  getSelling(id: number): Observable<Selling> {
    const url = `${this.sellingsUrl}/${id}`;
    return this.http.get<Selling>(url)
    .pipe(
      tap(_ => this.log(`fetched selling id=${id}`)),
      catchError(this.handleError<Selling>(`getSelling id=${id}`))
    );
  }

  addSelling(selling: Selling): Observable<Selling>{
    return this.http.post<Selling>(this.sellingsUrl, selling, this.httpOptions).pipe(
      tap((newSelling: Selling) => this.log(`added selling w/ id=${newSelling.id}`)),
      catchError(this.handleError<Selling>('addSelling'))
    );
  }

  deleteSelling(selling: Selling | number): Observable<Selling> {
    const id = typeof selling === 'number' ? selling : selling.id;
    const url = `${this.sellingsUrl}/${id}`;

    return this.http.delete<Selling>(url, this.httpOptions).pipe(
      tap(_ => this.log(`deleted selling id=${id}`)),
      catchError(this.handleError<Selling>('deleteSelling'))
    );
  }

  private log(message: string) {
    this.messageService.add(`SellingService: ${message}`);
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      this.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }
}
