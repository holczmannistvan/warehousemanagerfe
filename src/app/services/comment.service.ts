import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Comment } from '../models/comment';
import { MessageService } from './message.service';

@Injectable({
  providedIn: 'root'
})
export class CommentService {

  private commentsUrl = 'http://localhost:8080/comments';

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json'})
  };

  constructor(
    private http: HttpClient,
    private messageService: MessageService) { }

  getComments(): Observable<Comment[]> {
    return this.http.get<Comment[]>(this.commentsUrl)
      .pipe(
        tap(_ => this.log('fetchedcomments')),
        catchError(this.handleError<Comment[]>('getComments', []))
      );
  }

  getComment(id: number): Observable<Comment> {
    const url = `${this.commentsUrl}/${id}`;
    return this.http.get<Comment>(url)
    .pipe(
      tap(_ => this.log(`fetched comment id=${id}`)),
      catchError(this.handleError<Comment>(`getComment id=${id}`))
    );
  }

  addComment(comment: Comment): Observable<Comment>{
    return this.http.post<Comment>(this.commentsUrl, comment, this.httpOptions).pipe(
      tap((newComment: Comment) => this.log(`added comment w/ id=${newComment.id}`)),
      catchError(this.handleError<Comment>('addComment'))
    );
  }

  deleteComment(comment: Comment | number): Observable<Comment> {
    const id = typeof comment === 'number' ? comment : comment.id;
    const url = `${this.commentsUrl}/${id}`;

    return this.http.delete<Comment>(url, this.httpOptions).pipe(
      tap(_ => this.log(`deleted comment id=${id}`)),
      catchError(this.handleError<Comment>('deleteComment'))
    );
  }

  private log(message: string) {
    this.messageService.add(`CommentService: ${message}`);
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      this.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }
}
