import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { map, tap, catchError } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { MessageService } from './message.service';
import { User } from '../models/User';

@Injectable({
  providedIn: 'root'
})
export class BaseService {

  isLoggedIn = false;
  usersUrl = 'http://localhost:8080/users';
  loginUrl = 'http://localhost:8080/login';
  logoutUrl = 'http://localhost:8080/logout';

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json'})
  };


  user: User = {
      id: 0,
      username: 'unknown',
      password: '',
      role: ''
    };

  constructor(
    private http: HttpClient,
    private messageService: MessageService,
    private cookieService: CookieService) {
  }

  setUser(response: any, user: User) {
      user.id = response.id;
      user.username = response.username;
      user.password = response.password;
      user.role = response.role;
      return user;
  }

  init() {
    this.isLoggedIn = false;
    this.cookieService.delete('user');
    this.user = {
      id: 0,
      username: 'unknown',
      password: '',
      role: ''
    };
  }

  getByUsername(username: string){
    console.log(username);
    return this.http.get<User>(this.usersUrl + '/byUsername/?username=' + username);
  }

  getUserNames(): Observable<[]> {
    return this.http.get<[]>(this.usersUrl + '/usernames/')
      .pipe(
        tap(_ => this.log('fetched User Names')),
        catchError(this.handleError<[]>('getUserNames', []))
      );
  }

  getUsersAdmin(): Observable<User[]> {
    return this.http.get<User[]>(this.usersUrl)
      .pipe(
        tap(_ => this.log('fetchedUsers')),
        catchError(this.handleError<User[]>('getUsers', []))
      );
  }

  addUser(user: User): Observable<User>{
    return this.http.post<User>(this.usersUrl, user, this.httpOptions).pipe(
      tap((newUser: User) => this.log(`added user w/ id=${newUser.id}`)),
      catchError(this.handleError<User>('addUser'))
    );
  }

  deleteUser(user: User | number): Observable<User> {
    const id = typeof user === 'number' ? user : user.id;
    const url = `${this.usersUrl}/${id}`;

    return this.http.delete<User>(url, this.httpOptions).pipe(
      tap(_ => this.log(`deleted user id=${id}`)),
      catchError(this.handleError<User>('deleteUser'))
    );
  }

  updateUser(user: User): Observable<any> {
    const url = `${this.usersUrl}/${user.id}`;
    return this.http.put(url, user, this.httpOptions)
    .pipe(
      tap(_ => this.log(`updated user id=${user.id}`)),
      catchError(this.handleError<any>('updateUser'))
    );
  }

  login(user: User){
    return this.http.post<User>(this.loginUrl, { username: user.username, password: user.password }, this.httpOptions);
  }

  logout() {
    this.cookieService.set('user', '');
    this.cookieService.set('role', '');
  }

  private log(message: string) {
    this.messageService.add(`BaseService: ${message}`);
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      this.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }

}
