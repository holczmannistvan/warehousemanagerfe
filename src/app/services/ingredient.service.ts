import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Ingredient } from '../models/ingredient';
import { MessageService } from './message.service';

@Injectable({
  providedIn: 'root'
})
export class IngredientService {

  private ingredientsUrl = 'http://localhost:8080/ingredients';

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json'})
  };

  constructor(
    private http: HttpClient,
    private messageService: MessageService) { }

  getIngredients(): Observable<Ingredient[]> {
    return this.http.get<Ingredient[]>(this.ingredientsUrl)
      .pipe(
        tap(_ => this.log('fetchedingredients')),
        catchError(this.handleError<Ingredient[]>('getIngredients', []))
      );
  }

  getIngredient(id: number): Observable<Ingredient> {
    const url = `${this.ingredientsUrl}/${id}`;
    return this.http.get<Ingredient>(url)
    .pipe(
      tap(_ => this.log(`fetched ingredient id=${id}`)),
      catchError(this.handleError<Ingredient>(`getIngredient id=${id}`))
    );
  }

  searchIngredients(term: string): Observable<Ingredient[]>{
    if (!term.trim()){
      return of([]);
    }
    return this.http.get<Ingredient[]>(`${this.ingredientsUrl}/?name=${term}`).pipe(
      tap(x => x.length ?
        this.log(`found ingredients matching "${term}"` + `${this.ingredientsUrl}/?name=${term}`) : this.log(`no ingredients matching "${term}"`)),
      catchError(this.handleError<Ingredient[]>('seatchIngredients', []))
    );
  }

  updateIngredient(ingredient: Ingredient): Observable<any> {
    const url = `${this.ingredientsUrl}/${ingredient.id}`;
    return this.http.put(url, ingredient, this.httpOptions)
    .pipe(
      tap(_ => this.log(`updated ingredient id=${ingredient.id}`)),
      catchError(this.handleError<any>('updateIngredient'))
    );
  }

  addIngredient(ingredient: Ingredient): Observable<Ingredient>{
    return this.http.post<Ingredient>(this.ingredientsUrl, ingredient, this.httpOptions).pipe(
      tap((newIngredient: Ingredient) => this.log(`added ingredient w/ id=${newIngredient.id}`)),
      catchError(this.handleError<Ingredient>('addIngredient'))
    );
  }

  private log(message: string) {
    this.messageService.add(`IngredientService: ${message}`);
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      this.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }
}
