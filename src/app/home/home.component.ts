import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { BaseService } from '../services/base.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  title = 'Warehouse Manager';
  isAdmin: boolean;
  user: string;
  isLoggedIn: boolean;

  constructor(
    private cookieService: CookieService,
    private baseService: BaseService
  ) { }

  ngOnInit(): void {
    this.isLoggedIn = this.cookieService.get('user') !== '';
    if (!this.isLoggedIn) {
        window.location.replace('/login');
    }
    else {
      this.isAdmin = this.cookieService.get('role') === '1';
      this.user = this.cookieService.get('user');
    }
  }

  logout(): void {
    this.baseService.logout();
    window.location.replace('/login');
  }
}
