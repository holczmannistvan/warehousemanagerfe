import { Component, OnInit } from '@angular/core';

import { Ingredient } from '../../models/ingredient';
import { IngredientService } from '../../services/ingredient.service';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-ingredients',
  templateUrl: './ingredients.component.html',
  styleUrls: ['./ingredients.component.css']
})
export class IngredientsComponent implements OnInit {

  ingredients: Ingredient[];
  isLoggedIn: boolean;
  isAdmin: boolean;

  constructor(
    private ingredientService: IngredientService,
    private cookieService: CookieService
    ) {}

  ngOnInit(): void {
    this.isLoggedIn = this.cookieService.get('user') !== '';
    this.isAdmin = this.cookieService.get('role') === '1';
    if (!this.isLoggedIn) {
        window.location.replace('/login');
    }
    else {
      this.getIngredients();
    }
  }

  getIngredients(): void {
    this.ingredientService.getIngredients()
      .subscribe(ingredients => {
        if (this.isAdmin) {
          this.ingredients = ingredients;
        } else {
          this.ingredients = ingredients.filter(i => i.active);
        }
      });
  }

  enable(ingredient: Ingredient) {
    ingredient.active = true;
    this.ingredientService.updateIngredient(ingredient).subscribe();
  }

  disable(ingredient: Ingredient){
    if (!this.isAdmin) {
      this.ingredients = this.ingredients.filter(i => i !== ingredient);
    }
    ingredient.active = false;
    this.ingredientService.updateIngredient(ingredient).subscribe();
  }
}
