import { Component, OnInit } from '@angular/core';

import { Ingredient } from '../../models/ingredient';
import { IngredientService } from '../../services/ingredient.service';
import { Supplier } from '../../models/supplier';
import { SupplierService } from '../../services/supplier.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-ingredient-add',
  templateUrl: './ingredient-add.component.html',
  styleUrls: ['./ingredient-add.component.css']
})
export class IngredientAddComponent implements OnInit {

  suppliers: Supplier[];
  ingredientForm = new FormGroup({
    name: new FormControl('', Validators.required),
    cost: new FormControl('', Validators.required),
    quantity: new FormControl('', Validators.required),
    supplierName: new FormControl('', Validators.required),
  });

  constructor(
    private ingredientService: IngredientService,
    private supplierService: SupplierService) { }

  ngOnInit(): void {
    this.getSuppliers();
  }

  getSuppliers(): void {
    this.supplierService.getSuppliers()
      .subscribe(suppliers => this.suppliers = suppliers.filter(s => s.active));
  }

  add(): void {

    const name = this.ingredientForm.get('name').value;
    const cost = this.ingredientForm.get('cost').value;
    const quantity = this.ingredientForm.get('quantity').value;
    const supplier = this.ingredientForm.get('supplierName').value;
    const active = true;

    this.ingredientService.addIngredient({ active, name, cost, quantity, supplier } as unknown as Ingredient)
      .subscribe();
    window.location.reload();
  }
}
