import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Ingredient } from '../../models/ingredient';
import { IngredientService } from '../../services/ingredient.service';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-ingredient-detail',
  templateUrl: './ingredient-detail.component.html',
  styleUrls: ['./ingredient-detail.component.css']
})
export class IngredientDetailComponent implements OnInit {

  @Input() ingredient: Ingredient;
  isLoggedIn: boolean;

  constructor(
    private route: ActivatedRoute,
    private ingredientService: IngredientService,
    private location: Location,
    private cookieService: CookieService
  ) { }

  ngOnInit(): void {
    this.isLoggedIn = this.cookieService.get('user') !== '';
    if (!this.isLoggedIn) {
        window.location.replace('/login');
    }
    else {
      this.getIngredient();
    }
  }

  getIngredient(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.ingredientService.getIngredient(id)
      .subscribe(ingredient => this.ingredient = ingredient);
  }

  goBack(): void {
    this.location.back();
  }

  save(): void {
    this.ingredientService.updateIngredient(this.ingredient)
      .subscribe(() => this.goBack());
  }
}
