import { Component, OnInit, ViewChild } from '@angular/core';
import { from } from 'rxjs';
import { groupBy, map, flatMap, toArray } from 'rxjs/operators';
import * as _ from 'lodash';
import { CookieService } from 'ngx-cookie-service';


import { Ingredient } from '../models/ingredient';
import { IngredientService } from '../services/ingredient.service';
import { SellingService } from '../services/selling.service';
import { ProductService } from '../services/product.service';
import { RecipeService } from '../services/recipe.service';
import { Selling } from '../models/selling';
import { Product } from '../models/product';
import { Recipe } from '../models/recipe';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  isLoggedIn: boolean;
  isAdmin: boolean;

  type = 'pie';
  data = {
    labels: [],
    datasets: [
      {
        data: [],
        backgroundColor: []
      }
    ],
    loaded: false
  };
  options = {
    legend: { display: true,
              position: 'right'
            },
    responsive: true,
    maintainAspectRatio: true,
  };

  displayedColumns: string[] = ['grpBy', 'sum1', 'sum2', 'sum3', 'sum4'];
  displayed1Columns: string[] = ['name', 'price'];

  ingredients: Ingredient[];
  ingredientsToBuy: Ingredient[];

  sellings: Selling[];
  products: Product[];
  productsToProduce: Product[];

  recipes: Recipe[];
  cantProduce: Recipe[] = [];

  sellingsGrpByCustomer: any;
  sellingsGrpByProduct: any;
  productsGrpByName: any;
  costsGrpByProduct: any;

  costTable = [
    {name: 'Ingredients', price: 0},
    {name: 'Products', price: 0},
    {name: 'Selled Products', price: 0},
  ];

  profitTable = [
    {name: 'Profit', price: 0}
  ];

  constructor(
    private ingredientService: IngredientService,
    private sellingService: SellingService,
    private productService: ProductService,
    private recipeService: RecipeService,
    private cookieService: CookieService
    ) { }

  ngOnInit(): void {
    this.isLoggedIn = this.cookieService.get('user') !== '';
    this.isAdmin = this.cookieService.get('role') === '1';
    if (!this.isLoggedIn) {
        window.location.replace('/login');
    }
    else {
      this.getIngredientsDetails();
      this.getSellingsDetails();
      this.getProductsDetails();
      this.getRecipesDetails();
    }
  }

  getIngredientsDetails(): void {
    this.ingredientService.getIngredients()
      .subscribe(ingredients => {
        ingredients.forEach(ingredient => {
          this.costTable[0].price += ingredient.cost * ingredient.quantity;
        });
        this.ingredients = ingredients;
        this.ingredientsToBuy = this.ingredients.filter(ingredient => ingredient.quantity <= 5 && ingredient.active);
      });
  }

  getRecipesDetails(): void {
    this.recipeService.getRecipes()
      .subscribe(recipes => {
        this.recipes = recipes;
        recipes.forEach(recipe => {
          let producable = true;
          recipe.ingredients.forEach(ingredient => {
            if (ingredient.quantity === 0 ) {
              producable = producable && false;
            }
          });
          if (!producable) {
            this.cantProduce.push(recipe);
          }
        });
      });
  }

  getProductsDetails(): void {
    const rgbArray = Array.from(Array(255).keys());
    let rnd = _.sampleSize(rgbArray, 3);
    this.productService.getProducts()
      .subscribe(products => {
        this.products = products;
        this.productsToProduce = this.products.filter(product => product.quantity <= 5 && product.active);
        this.products.forEach(product => {
          this.costTable[1].price += product.quantity * product.price;
          this.data.datasets[0].data.push(product.quantity);
          this.data.labels.push(product.name);
          this.data.datasets[0].backgroundColor.push('rgb(' + rnd[0] + ', ' + rnd[1] + ', ' + rnd[2] + ')');
          rnd = _.sampleSize(rgbArray, 3);
        });
        this.data.loaded = true;
        this.productsGrpByName = products;
      });
  }

  getGrpBy<T>(table: T[], grpBy: Array<string>, columns: Array<string>): any {

    let grpByFinal = [];
    const boolGrpBy = grpBy.length === 1;
    const source = from(table);

    const grpByItem = source.pipe(
      groupBy(tableItem => {
        if (boolGrpBy) {
          return tableItem[grpBy[0]];
        } else {
          return tableItem[grpBy[0]][grpBy[1]];
        }}
      ),
      // return each item in group as array
      flatMap(group => group.pipe(toArray())),
      map(g => {
        const obj = [{grpBy: g[0][grpBy[0]]}];
        let idx = 1;
        columns.forEach(col => {
          obj[0]['sum' + idx] = _.sumBy(g, col);
          idx += 1;
        });
        return obj;

        })
    );

    grpByItem.subscribe(val => {
      if (grpByFinal === undefined) {
        grpByFinal = val;
      } else {
        grpByFinal.push(val[0]);
      }
    });

    return grpByFinal;

  }

  getSellingsDetails(): void {
    this.sellingService.getSellings()
      .subscribe(sellings => {
        sellings.forEach(selling => {
          selling.cost = selling.quantity * selling.product.price;
          selling.profit = selling.earning - selling.cost;
          this.costTable[2].price  += selling.cost;
          this.profitTable[0].price  += selling.earning - selling.cost;
        });
        this.sellings = sellings;

        this.sellingsGrpByCustomer = this.getGrpBy(sellings, ['customerGroup'], ['quantity', 'earning', 'cost', 'profit']);
        this.sellingsGrpByProduct = this.getGrpBy(sellings, ['product', 'name'], ['quantity', 'earning', 'cost', 'profit']);

      });
  }
}
