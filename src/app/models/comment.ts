import { Recipe } from './recipe';

export interface Comment {
    id: number;
    comment: string;
    recipe: Recipe;
}
