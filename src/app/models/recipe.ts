import { Ingredient } from './ingredient';
import { Comment } from './comment';


export interface Recipe {
    id: number;
    active: boolean;
    name: string;
    cost: number;
    comment: Comment[];
    ingredients: Ingredient[];
}
