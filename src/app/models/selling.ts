import { Product } from './product';

export interface Selling {
    id: number;
    customerGroup: string;
    earning: number;
    quantity: number;
    product: Product;
    cost: number;
    profit: number;
}
