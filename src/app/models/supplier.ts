export interface Supplier {
    id: number;
    active: boolean;
    name: string;
    billingAddress: string;
    accountNumber: number;
}
