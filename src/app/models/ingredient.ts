import { Supplier } from './supplier';
import { Recipe } from './recipe';

export interface Ingredient {
    id: number;
    active: boolean;
    name: string;
    cost: number;
    quantity: number;
    brand: number;
    ingredientGroup: number;
    supplier: Supplier;
    recipes: Recipe[];
}
