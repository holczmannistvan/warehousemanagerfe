import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { RecipeDetailComponent } from '../recipe-detail/recipe-detail.component';

import { MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Inject } from '@angular/core';

import { Comment } from '../../models/comment';
import { CommentService } from '../../services/comment.service';

@Component({
  selector: 'app-comment-add',
  templateUrl: './comment-add.component.html',
  styleUrls: ['./comment-add.component.css']
})
export class CommentAddComponent implements OnInit {

  constructor(
    private commentService: CommentService,
    public dialogRef: MatDialogRef<RecipeDetailComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit(): void {
  }

  delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
  }

  onSubmit(comment: string){
    this.dialogRef.close();
    const recipe = this.data.recipe;
    this.commentService.addComment({ comment, recipe } as unknown as Comment)
        .subscribe();

  }

  onClose(): void {
    this.dialogRef.close();
  }

}
