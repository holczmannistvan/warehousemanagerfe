import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

import { RecipeDetailComponent } from '../recipe-detail/recipe-detail.component';

@Component({
  selector: 'app-recipe-warning',
  templateUrl: './recipe-warning.component.html',
  styleUrls: ['./recipe-warning.component.css']
})
export class RecipeWarningComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<RecipeDetailComponent>
    ) { }

  ngOnInit(): void {
  }

  onClose(): void {
    this.dialogRef.close();
  }

}
