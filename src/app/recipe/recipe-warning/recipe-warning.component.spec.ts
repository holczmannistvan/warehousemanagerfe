import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecipeWarningComponent } from './recipe-warning.component';

describe('RecipeWarningComponent', () => {
  let component: RecipeWarningComponent;
  let fixture: ComponentFixture<RecipeWarningComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecipeWarningComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecipeWarningComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
