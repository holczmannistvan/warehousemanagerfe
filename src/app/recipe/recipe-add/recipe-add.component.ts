import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { RecipeAddIngredientsComponent } from '../recipe-add-ingredients/recipe-add-ingredients.component';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { Recipe } from '../../models/recipe';
import { RecipeService } from '../../services/recipe.service';

@Component({
  selector: 'app-recipe-add',
  templateUrl: './recipe-add.component.html',
  styleUrls: ['./recipe-add.component.css']
})
export class RecipeAddComponent implements OnInit {

  private recipes: Recipe[];
  recipeForm = new FormGroup({
    name: new FormControl('', Validators.required)
  });

  constructor(
    private recipeService: RecipeService,
    private dialog: MatDialog,
  ) { }

  ngOnInit(): void {
    this.getRecipes();
  }

  getRecipes(): void {
    this.recipeService.getRecipes()
      .subscribe(recipes => this.recipes = recipes);
  }

  findRecipeByName(name: string): Recipe{
    return this.recipes.find(r => r.name === name);
  }

  delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
  }

  add(name: string): void {
    const active = true;
    this.recipeService.addRecipe({ name, active } as unknown as Recipe)
      .subscribe();
  }

  onCreate() {

    const name = this.recipeForm.get('name').value;
    const availRecipe = this.findRecipeByName(name);

    if (availRecipe !== undefined){
      window.location.replace('/recipes/detail/' + availRecipe.id);
    } else {
      (async () => {
        this.add(name);
        await this.delay(100);
        this.getRecipes();
        await this.delay(100);

        const recipe = this.findRecipeByName(name);

        const dialogConfig = new MatDialogConfig();
        dialogConfig.autoFocus = true;
        dialogConfig.width = '60%';
        dialogConfig.data = {recipe};

        const dialogRef = this.dialog.open(RecipeAddIngredientsComponent, dialogConfig);
        dialogRef.afterClosed().subscribe( _ => window.location.replace('/recipes/detail/' + recipe.id));
      })();
    }
  }

}
