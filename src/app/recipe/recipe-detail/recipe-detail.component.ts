import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { CookieService } from 'ngx-cookie-service';

import { Recipe } from '../../models/recipe';
import { RecipeService } from '../../services/recipe.service';
import { ProductService } from '../../services/product.service';
import { Product } from '../../models/product';
import { IngredientService } from '../../services/ingredient.service';
import { RecipeWarningComponent } from '../recipe-warning/recipe-warning.component';
import { CommentAddComponent } from '../comment-add/comment-add.component';
import { CommentService } from '../../services/comment.service';
import { Comment } from '../../models/comment';

@Component({
  selector: 'app-recipe-detail',
  templateUrl: './recipe-detail.component.html',
  styleUrls: ['./recipe-detail.component.css']
})
export class RecipeDetailComponent implements OnInit {

  @Input() recipe: Recipe;
  possibleCreation = true;
  products: Product[];
  comments: Comment[];
  isLoggedIn: boolean;

  constructor(
    private route: ActivatedRoute,
    private recipeService: RecipeService,
    private productService: ProductService,
    private ingredientService: IngredientService,
    private commentService: CommentService,
    private location: Location,
    private dialog: MatDialog,
    private cookieService: CookieService
  ) { }

  ngOnInit(): void {
    this.isLoggedIn = this.cookieService.get('user') !== '';
    if (!this.isLoggedIn) {
        window.location.replace('/login');
    }
    else {
      this.getRecipe();
      this.getProducts();
    }
  }

  getRecipe(): Recipe {
    const id = +this.route.snapshot.paramMap.get('id');
    this.recipeService.getRecipe(id)
      .subscribe(recipe => this.recipe = recipe);

    return this.recipe;
  }

  getRelevantComments(): void {
    this.commentService.getComments()
      .subscribe(comments => this.comments = comments.filter(
        comment => comment.recipe.id === this.recipe.id
      ).reverse());

  }

  getProducts(): void {
    this.productService.getProducts().subscribe(products => this.products = products);
  }

  goBack(): void {
    this.location.back();
  }

  save(): void {
    this.recipeService.updateRecipe(this.recipe)
      .subscribe(() => this.goBack());
  }

  deleteComment(comment: Comment){
    this.comments = this.comments.filter(c => c !== comment);
    this.commentService.deleteComment(comment).subscribe();
  }

  checkCreation(): void{
    this.recipe.ingredients.forEach(ingredient => {
        ingredient.quantity === 0 ? this.possibleCreation = false : this.possibleCreation = this.possibleCreation; });
  }

  addComment(): void {

    const recipe = this.recipe;

    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.width = '60%';
    dialogConfig.data = {recipe};

    this.dialog.open(CommentAddComponent, dialogConfig);
  }

  createProduct(): void {

    const name = this.recipe.name;
    const quantity = 30;
    const price = this.recipe.cost / quantity;
    const active = true;
    this.checkCreation();
    if (this.possibleCreation){
      this.recipe.ingredients.forEach(ingredient => {
        ingredient.quantity -= 1;
        this.ingredientService.updateIngredient(ingredient).subscribe();
      });
      const actProduct = this.products.find(product => (product.name === name));
      if (actProduct !== undefined){
        actProduct.quantity += quantity;
        this.productService.updateProduct(actProduct).subscribe();
      } else {
        this.productService.addProduct({active, name, price, quantity } as unknown as Product)
        .subscribe();
      }
      window.location.replace('/products/');
    }else {
      const dialogConfig = new MatDialogConfig();
      dialogConfig.disableClose = true;
      dialogConfig.autoFocus = true;
      dialogConfig.width = '60%';

      this.dialog.open(RecipeWarningComponent, dialogConfig);
    }
  }
}
