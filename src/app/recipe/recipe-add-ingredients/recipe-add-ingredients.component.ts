import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Inject } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { IngredientService } from '../../services/ingredient.service';
import { Ingredient } from '../../models/ingredient';
import { RecipeService } from '../../services/recipe.service';

@Component({
  selector: 'app-recipe-add-ingredients',
  templateUrl: './recipe-add-ingredients.component.html',
  styleUrls: ['./recipe-add-ingredients.component.css']
})
export class RecipeAddIngredientsComponent implements OnInit {

  ingredients: Ingredient[];
  addedIngredientsForm = new FormGroup({
    addedIngredients: new FormControl('', Validators.required),
  });

  constructor(
    public dialogRef: MatDialogRef<RecipeAddIngredientsComponent>,
    private ingredientService: IngredientService,
    private recipeService: RecipeService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit(): void {
    this.getIngredients();
  }

  getIngredients(): void {
    this.ingredientService.getIngredients()
      .subscribe(ingredients => this.ingredients = ingredients.filter(i => i.active));
  }

  onSubmit(): void {
    this.dialogRef.close();
    const addedIngredients = this.addedIngredientsForm.get('addedIngredients').value;
    console.log(addedIngredients);
    let cost = 0;
    addedIngredients.forEach(ingredient => {
      this.data.recipe.ingredients.push(ingredient);
      cost += ingredient.cost;
    });
    this.data.recipe.cost = cost;
    this.recipeService.updateRecipe(this.data.recipe).subscribe();
  }
}
