import { Component, OnInit } from '@angular/core';

import { Recipe } from '../../models/recipe';
import { RecipeService } from '../../services/recipe.service';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-recipes',
  templateUrl: './recipes.component.html',
  styleUrls: ['./recipes.component.css']
})
export class RecipesComponent implements OnInit {

  recipes: Recipe[];
  isLoggedIn: boolean;
  isAdmin: boolean;

  constructor(
    private recipeService: RecipeService,
    private cookieService: CookieService
    ) { }

  ngOnInit(): void {
    this.isLoggedIn = this.cookieService.get('user') !== '';
    this.isAdmin = this.cookieService.get('role') === '1';
    if (!this.isLoggedIn) {
        window.location.replace('/login');
    }
    else {
      this.getRecipes();
    }
  }

  getRecipes(): void {
    this.recipeService.getRecipes()
      .subscribe( recipes => {
        if (this.isAdmin) {
          this.recipes = recipes;
        } else {
          this.recipes = recipes.filter(r => r.active);
        }
      });
  }

  enable(recipe: Recipe) {
    recipe.active = true;
    this.recipeService.updateRecipe(recipe).subscribe();
  }

  disable(recipe: Recipe){
    if (!this.isAdmin) {
      this.recipes = this.recipes.filter(r => r !== recipe);
    }
    recipe.active = false;
    this.recipeService.updateRecipe(recipe).subscribe();
  }
}
