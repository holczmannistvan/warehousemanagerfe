import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

import { BaseService } from '../services/base.service';
import { User } from '../models/User';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  displayedColumns: string[] = ['username', 'password', 'role', 'actions'];

  users: User[];
  isLoggedIn: boolean;

  constructor(
    private baseService: BaseService,
    private cookieService: CookieService
  ) { }

  ngOnInit(): void {
    this.isLoggedIn = this.cookieService.get('user') !== '';
    if (!this.isLoggedIn) {
        window.location.replace('/login');
    }
    else {
      this.getUsers();
    }
  }

  getUsers(): void {
    this.baseService.getUsersAdmin()
      .subscribe( users => this.users = users);
  }

  delete(user: User){
    this.users = this.users.filter(u => u !== user);
    this.baseService.deleteUser(user).subscribe();
  }

  setAdmin(user: User){
    user.role = 'ADMIN';
    this.baseService.updateUser(user).subscribe(() =>  window.location.reload());
  }

  setUser(user: User){
    user.role = 'USER';
    this.baseService.updateUser(user).subscribe(() =>  window.location.reload());
  }

}
