import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './dashboard/dashboard.component';
import { IngredientsComponent } from './ingredient/ingredients/ingredients.component';
import { IngredientDetailComponent } from './ingredient/ingredient-detail/ingredient-detail.component';
import { SuppliersComponent } from './supplier/suppliers/suppliers.component';
import { SupplierDetailComponent } from './supplier/supplier-detail/supplier-detail.component';
import { RecipesComponent } from './recipe/recipes/recipes.component';
import { RecipeDetailComponent } from './recipe/recipe-detail/recipe-detail.component';
import { ProductsComponent } from './product/products/products.component';
import { ProductDetailComponent } from './product/product-detail/product-detail.component';
import { LoginComponent } from './login/login.component';
import { UsersComponent } from './users/users.component';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent},
  { path: 'dashboard', component: DashboardComponent},
  { path: 'ingredients', component: IngredientsComponent },
  { path: 'ingredients/detail/:id', component: IngredientDetailComponent },
  { path: 'suppliers', component: SuppliersComponent },
  { path: 'suppliers/detail/:id', component: SupplierDetailComponent },
  { path: 'recipes', component: RecipesComponent },
  { path: 'recipes/detail/:id', component: RecipeDetailComponent},
  { path: 'products', component: ProductsComponent},
  { path: 'products/detail/:id', component: ProductDetailComponent},
  { path: 'users', component: UsersComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
