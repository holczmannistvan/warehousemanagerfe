import { Component, OnInit } from '@angular/core';
import { User } from '../models/User';
import { BaseService } from '../services/base.service';
import { CookieService } from 'ngx-cookie-service';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  message = '';

  title = 'login';
  user: User = {
    id: 0,
    username: '',
    password: '',
    role: ''
  };

  loginForm = new FormGroup({
    username: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required)
  });
  isLoggedIn: boolean;

  constructor(
    public baseService: BaseService,
    private cookieService: CookieService,
  ) { }

  ngOnInit(): void {
    this.isLoggedIn = this.cookieService.get('user') !== '';
    if (this.isLoggedIn) {
        window.location.replace('/dashboard');
    }
  }


  login() {
    this.user.username = this.loginForm.get('username').value;
    this.user.password = this.loginForm.get('password').value;

    this.baseService.login(this.user).subscribe( response => {
      if (response != null) {

        this.cookieService.set('user', response.username);
        const isAdmin = response.role === 'ADMIN' ? '1' : '0';
        this.cookieService.set('role', isAdmin);

        console.log('successful login');
        window.location.replace('/dashboard');
      }
      else {
        console.log('unsuccessful login: ' + this.user.username + ' ' + this.user.password);
        alert('Invalid login credentials');
      }
    }, _ => {
      alert('Internal server error.');
    });
  }

  register(): void {
    const username = this.loginForm.get('username').value;
    const password = this.loginForm.get('password').value;
    let role = 'USER';

    this.baseService.getUserNames().subscribe( usernames => {
      if (usernames.length === 0) {
        role = 'ADMIN';
      }
      if (usernames.some(x => x === username)) {
        alert('This username is already exist. Please chose another one.');
      } else if (username === '') {
        alert('Please choose a username, no name is not allowed!');
      } else if (password === '') {
        alert('Please add a password!');
      } else {
        this.baseService.addUser({ username, password, role } as unknown as User)
          .subscribe( user => {
            this.cookieService.set('user', user.username);
            const isAdmin = user.role === 'ADMIN' ? '1' : '0';
            this.cookieService.set('role', isAdmin);

            window.location.replace('/dashboard');
          });
      }
    });
  }
}
