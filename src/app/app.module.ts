import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';

import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatDialogModule} from '@angular/material/dialog';
import { ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import {MatTableModule} from '@angular/material/table';

import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import { LayoutModule } from '@angular/cdk/layout';
registerLocaleData(localeFr, 'fr');

import { ChartModule } from 'angular2-chartjs';
import { CookieService } from 'ngx-cookie-service';

import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { UsersComponent } from './users/users.component';

import { MessagesComponent } from './messages/messages.component';
import { IngredientsComponent } from './ingredient/ingredients/ingredients.component';
import { IngredientDetailComponent } from './ingredient/ingredient-detail/ingredient-detail.component';
import { IngredientSearchComponent } from './ingredient/ingredient-search/ingredient-search.component';
import { IngredientAddComponent } from './ingredient/ingredient-add/ingredient-add.component';

import { SuppliersComponent } from './supplier/suppliers/suppliers.component';
import { SupplierDetailComponent } from './supplier/supplier-detail/supplier-detail.component';
import { SupplierAddComponent } from './supplier/supplier-add/supplier-add.component';

import { RecipesComponent } from './recipe/recipes/recipes.component';
import { RecipeDetailComponent } from './recipe/recipe-detail/recipe-detail.component';
import { RecipeAddComponent } from './recipe/recipe-add/recipe-add.component';
import { RecipeAddIngredientsComponent } from './recipe/recipe-add-ingredients/recipe-add-ingredients.component';
import { RecipeWarningComponent } from './recipe/recipe-warning/recipe-warning.component';
import { CommentAddComponent } from './recipe/comment-add/comment-add.component';

import { ProductsComponent } from './product/products/products.component';
import { ProductDetailComponent } from './product/product-detail/product-detail.component';
import { ProductSearchComponent } from './product/product-search/product-search.component';

import { ProductSellComponent } from './product/product-sell/product-sell.component';
import { SellingMessageComponent } from './product/selling-message/selling-message.component';


@NgModule({
  declarations: [
    AppComponent,
    IngredientsComponent,
    IngredientDetailComponent,
    MessagesComponent,
    DashboardComponent,
    IngredientSearchComponent,
    SuppliersComponent,
    IngredientAddComponent,
    SupplierDetailComponent,
    SupplierAddComponent,
    RecipesComponent,
    RecipeDetailComponent,
    RecipeAddComponent,
    ProductsComponent,
    ProductDetailComponent,
    ProductSearchComponent,
    RecipeAddIngredientsComponent,
    RecipeWarningComponent,
    CommentAddComponent,
    ProductSellComponent,
    SellingMessageComponent,
    LoginComponent,
    HomeComponent,
    UsersComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    MDBBootstrapModule.forRoot(),
    BrowserAnimationsModule,
    MatDialogModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatFormFieldModule,
    MatInputModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    LayoutModule,
    MatTableModule,
    ChartModule
  ],
  exports: [
    MatDialogModule,
    MatFormFieldModule
  ],
  providers: [CookieService],
  bootstrap: [AppComponent],
  entryComponents: [RecipeAddIngredientsComponent, RecipeWarningComponent, CommentAddComponent,
    ProductSellComponent, SellingMessageComponent]
})
export class AppModule { }
