import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

import { Product } from '../../models/product';
import { ProductService } from '../../services/product.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  products: Product[];
  isLoggedIn: boolean;
  isAdmin: boolean;

  constructor(
    private productService: ProductService,
    private cookieService: CookieService
    ) {}

  ngOnInit(): void {
    this.isLoggedIn = this.cookieService.get('user') !== '';
    this.isAdmin = this.cookieService.get('role') === '1';
    if (!this.isLoggedIn) {
        window.location.replace('/login');
    }
    else {
      this.getProducts();
    }
  }

  getProducts(): void {
    this.productService.getProducts()
      .subscribe(products => {
        if (this.isAdmin) {
          this.products = products;
        } else {
          this.products = products.filter(p => p.active);
        }
      });
  }

  enable(product: Product) {
    product.active = true;
    this.productService.updateProduct(product).subscribe();
  }

  disable(product: Product){
    if (!this.isAdmin) {
      this.products = this.products.filter(p => p !== product);
    }
    product.active = false;
    this.productService.updateProduct(product).subscribe();
  }
}
