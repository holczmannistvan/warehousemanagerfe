import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { MatDialog, MatDialogConfig } from '@angular/material/dialog';

import { Product } from '../../models/product';
import { ProductService } from '../../services/product.service';
import { ProductSellComponent } from '../product-sell/product-sell.component';
import { Selling } from '../../models/selling';
import { SellingService } from '../../services/selling.service';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {

  displayedColumns: string[] = ['customerGroup', 'quantity', 'earning', 'actions'];

  @Input() product: Product;
  sellings: Selling[];
  isLoggedIn: boolean;

  constructor(
    private route: ActivatedRoute,
    private productService: ProductService,
    private sellingService: SellingService,
    private location: Location,
    private dialog: MatDialog,
    private cookieService: CookieService
  ) { }

  ngOnInit(): void {
    this.isLoggedIn = this.cookieService.get('user') !== '';
    if (!this.isLoggedIn) {
        window.location.replace('/login');
    }
    else {
      this.getProduct();
    }
  }

  getProduct(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.productService.getProduct(id)
      .subscribe(product => this.product = product);
  }

  getRelevantSellings(): void {
    this.sellingService.getSellings()
      .subscribe(sellings => this.sellings = sellings.filter(
        selling => selling.product.id === this.product.id
      ).reverse());
  }

  goBack(): void {
    this.location.back();
  }

  save(): void {
    this.productService.updateProduct(this.product)
      .subscribe(() => this.goBack());
  }

  delete(selling: Selling){
    this.sellings = this.sellings.filter(s => s !== selling);
    this.sellingService.deleteSelling(selling).subscribe();
  }

  sellProduct(): void {
    const product = this.product;

    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.width = '60%';
    dialogConfig.data = {product};

    this.dialog.open(ProductSellComponent, dialogConfig);
  }
}
