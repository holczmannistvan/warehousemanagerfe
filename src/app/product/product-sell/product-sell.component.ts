import { Component, OnInit } from '@angular/core';

import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatDialogRef } from '@angular/material/dialog';
import { MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Inject } from '@angular/core';

import { FormControl, Validators, FormGroup } from '@angular/forms';

import { ProductDetailComponent } from '../product-detail/product-detail.component';
import { ProductService } from '../../services/product.service';
import { SellingService } from '../../services/selling.service';
import { Selling } from '../../models/selling';
import { SellingMessageComponent } from '../selling-message/selling-message.component';

@Component({
  selector: 'app-product-sell',
  templateUrl: './product-sell.component.html',
  styleUrls: ['./product-sell.component.css']
})
export class ProductSellComponent implements OnInit {

  groups: any;
  sellingForm = new FormGroup({
    quantity: new FormControl('', Validators.required),
    addedGroup: new FormControl('', Validators.required)
  });

  constructor(
    public dialogRef: MatDialogRef<ProductDetailComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private productService: ProductService,
    private sellingService: SellingService,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.setGroups();
  }

  setGroups(): void {
    this.groups = [
      {id: 1, name: 'Customer', value: 0.1},
      {id: 2, name: 'Buyer', value: 0},
      {id: 3, name: 'Wholesaler', value: 0.3}
    ];
  }

  checkSelling(sellingQuantity: number): boolean {
    return this.data.product.quantity >= sellingQuantity;
  }

  onSubmit(): void {
    this.dialogRef.close();
    let message = '';
    let address = '';
    const quantity = this.sellingForm.get('quantity').value;
    const addedGroup = this.sellingForm.get('addedGroup');
    if (this.checkSelling(quantity)) {
      address = '/products';
      this.data.product.quantity -= quantity;
      this.productService.updateProduct(this.data.product).subscribe();

      const customerGroup = addedGroup.value.name;
      const earning = this.data.product.price * 2 * (1 - addedGroup.value.value) * quantity;
      const product = this.data.product;

      message = 'Quantity of product is sucessfully decreased with ' + quantity + '. Total earnings on this selling: ' + earning + ' Ft.';

      this.sellingService.addSelling({customerGroup, earning, quantity, product} as unknown as Selling)
        .subscribe();

    } else {
      message = 'There are not enough products to sell, please produce.';
      address = '/products/detail/' + this.data.product.id;
    }
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '60%';
    dialogConfig.data = {message};

    const dialogRef = this.dialog.open(SellingMessageComponent, dialogConfig);
    dialogRef.afterClosed().subscribe( _ => window.location.replace(address));
  }
  onClose(): void {
    this.dialogRef.close();
  }

}
