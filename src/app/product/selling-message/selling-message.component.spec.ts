import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SellingWarningComponent } from './selling-warning.component';

describe('SellingWarningComponent', () => {
  let component: SellingWarningComponent;
  let fixture: ComponentFixture<SellingWarningComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellingWarningComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellingWarningComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
