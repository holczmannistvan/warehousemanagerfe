import { Component, OnInit } from '@angular/core';

import { MatDialogRef } from '@angular/material/dialog';
import { MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Inject } from '@angular/core';

import { ProductSellComponent } from '../product-sell/product-sell.component';

@Component({
  selector: 'app-selling-warning',
  templateUrl: './selling-message.component.html',
  styleUrls: ['./selling-message.component.css']
})
export class SellingMessageComponent implements OnInit {

  message: string;
  constructor(
    public dialogRef: MatDialogRef<ProductSellComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit(): void {
    this.message = this.data.message;
  }

  onClose(): void {
    this.dialogRef.close();
  }

}
